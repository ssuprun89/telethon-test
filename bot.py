import argparse
import asyncio
import datetime
import time

from telethon import TelegramClient, events, sync
from telethon.tl.functions.channels import InviteToChannelRequest

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--id')
parser.add_argument('-ha', '--hash')
parser.add_argument('-s', '--session')
parser.add_argument('-p', '--page')
parser.add_argument('-sl', '--sleep')
args = parser.parse_args()

# These example values won't work. You must get your own api_id and
# api_hash from https://my.telegram.org, under API Development.
api_id = args.id
api_hash = args.hash

client = TelegramClient(args.session, api_id, api_hash).start()

channel = client.get_entity('benzin_dnepr')

members = client.get_participants(channel, limit=2000)[500 * int(args.page): 500 * (int(args.page) + 1)]

list_username = [user.username for user in members if user.username]


for user in list_username:
    try:
        client(InviteToChannelRequest(
            'pogoda_dnepr',
            [user]
        ))
        print(f'[{datetime.datetime.now()}] added: {user}')
        time.sleep(60)
    except Exception as e:
        print(e)